# Rendu "Injection"

## Binome

    - Boumahdi Samy
    - Baldé Ibrahima

## Question 1

- Quel est ce mécanisme?

Le mécanisme mis en place pour tenter d'empecher l'exploitation de la vulnérabilité est l'utilisation d'une variable expression régilière aussi appelé regex.

- Est-il efficace? Pourquoi?

var regex = /^[a-zA-Z0-9]+$/;

Ce mécanise est efficace car on veut une syntaxe précise, un ensemble de chaînes de caractères possibles et rien d'autres (dans notre cas un mot composé uniquement de ces caractères [a-zA-Z0-9]).

## Question 2

- Votre commande curl

curl -d 'chaine= Hello World' -X POST http://localhost:8080/

On a inséré des caractères normalement interdit avec le caractère "espace" entre Hello et World.

## Question 3

- Votre commande curl pour tout en faisant en sorte que le champ who soit rempli avec ce que vous aurez décidé (et non pas votre adresse IP)

curl -d "chaine=mon site web','google.com') -- " -X POST http://localhost:8080/

- Expliquez comment obtenir des informations sur une autre table

On doit finir la requête SQL en cours et introduire une seconde requête qui nous affiche des infos sur les tables, SELECT \* from <nom_table>, après avoir préalablement regardé le nom des tables existantes.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Au préalable, on indique qu'on accepte les requêtes préparées. On a passé le paramètre %s en dur pour nos valeurs. Ainsi on évite les injections sql.

L'attribut txt contiendra tout le contenu de notre variable chaine de la commande curl. (post["chaine"])

Tandis que l'attribut who est lui aussi fixe est inséré après. (cherrypy.request.remote.ip)

## Question 5

- Commande curl pour afficher une fenetre de dialog.

curl -d chaine="<script>alert('test')</script>" 'http://localhost:8080/'

(pb d'affichage dans le readme sur la commande ci dessus dans la variable chaine, j'ouvre une balise script dans laquelle je fais un alert("msg"))

- Commande curl pour lire les cookies

nc -l -p <numero de port>

curl -d chaine="<script>alert(document.cookie)</script>" 'http://localhost:8080/'

(pb d'affichage dans le readme sur la commande ci dessus dans la variable chaine, j'ouvre une balise script dans laquelle je fais un alert(document.cookie))

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

On utilise html.escape qui permet comme son nom l'indique d'échapper les balises html et de lire seulement le contenu de "chaines".

Nous realisons ce traitement au moment de l'insertion dans la base de donnée pour eviter d'inserer un script
